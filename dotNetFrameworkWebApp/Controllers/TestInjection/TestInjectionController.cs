﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace dotNetFrameworkWebApp.Controllers.TestInjection
{
    public class TestInjectionController: ApiController
    {
        // GET api/TestInjection
        public string Get()
        {
            return "you are in TestInjection";
        }
    }
}