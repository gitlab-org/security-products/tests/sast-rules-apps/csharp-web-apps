﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;

//ref: csharp_endpoint_rule-UnvalidatedRedirect
namespace dotNetFrameworkWebApp.Controllers.Endpoint
{
    public class UnvalidatedRedirectController : Controller
    {
        // GET https://localhost:44355/UnvalidatedRedirect/Get?path=testPoint
        public ActionResult Get(string path)
        {
            string url = Url.HttpRouteUrl(path, null);
            // ok: csharp_endpoint_rule-UnvalidatedRedirect
            return Redirect(url);
        }

    }
}