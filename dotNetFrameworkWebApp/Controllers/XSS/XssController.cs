﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.UI;
using System.Web.WebPages;

// ref: csharp_xss_rule-HtmlElementXss
namespace dotNetFrameworkWebApp.Controllers.XSS
{
    public class XssController : Controller
    {
        // sample: http://172.21.156.246/xss?alert=alert
        public ActionResult Index(string alert)
        {
            Response.AddHeader("Alert-Header", alert + "('From AddHeader')");
            Response.AppendHeader("Alert-Header2", alert + "('From AppendHeader')");
            // ruleid: csharp_xss_rule-HtmlElementXss
            Response.Write(alert);
            return View();
        }

        [HttpPost]
        public ActionResult HandleButtonClick(string inputScript)
        {
            string strForWrite = HttpUtility.HtmlAttributeEncode("<script>" + inputScript + "('From HtmlTextWriter WriteFullBeginTag')</script>");
            // ok: csharp_xss_rule-HtmlElementXss
            Response.Write(strForWrite);
            return View();
        }

        // sample: http://172.21.156.246/xss/WriteHtml
        public ActionResult WriteHtml(string alert)
        {
            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(stringWriter);

            string sanitize = HttpUtility.HtmlEncode("<script>" + alert + "('From HtmlTextWriter WriteFullBeginTag')</script>");
            // ok: csharp_xss_rule-HtmlElementXss
            writer.WriteFullBeginTag(sanitize);

            // ruleid: csharp_xss_rule-HtmlElementXss
            writer.Write("<script>" + alert + "('From HtmlTextWriter Write')</script>");

            // ruleid: csharp_xss_rule-HtmlElementXss
            writer.WriteBeginTag("<script>"+ alert + "('From HtmlTextWriter WriteBeginTag')</script>");

            // ruleid: csharp_xss_rule-HtmlElementXss
            writer.WriteEndTag("<script><script>"+ alert + "('From HtmlTextWriter WriteEndTag')</script></script>");

            // ruleid: csharp_xss_rule-HtmlElementXss
            writer.RenderBeginTag("<script>"+ alert + "('From HtmlTextWriter RenderBeginTag')</script>");

            writer.EndRender();

            // ruleid: csharp_xss_rule-HtmlElementXss
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "\"><script>"+ alert + "('From HtmlTextWriter AddAttribute')</script>");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.Write("Content inside the span");
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            // ruleid: csharp_xss_rule-HtmlElementXss
            writer.WriteAttribute("class", "''><script>"+ alert + "('From HtmlTextWriter WriteAttribute')</script>");
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            // ruleid: csharp_xss_rule-HtmlElementXss
            writer.AddStyleAttribute("color", "\"><script>"+ alert + "('From HtmlTextWriter AddStyleAttribute')</script>");
            writer.EndRender();

            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            // ruleid: csharp_xss_rule-HtmlElementXss
            writer.WriteStyleAttribute("color", "\"><script>"+ alert + "('From HtmlTextWriter WriteStyleAttribute')</script>");
            writer.EndRender();

            writer.Flush();
            return Content(stringWriter.ToString(), "text/html");
        }

        // sample: http://172.21.156.246/xss/BinaryWrite
        public void BinaryWrite()
        {
                string imagePath = Server.MapPath("~/Content/Html/html1.html");
                Response.ContentType = "text/html";
                Response.BinaryWrite(System.IO.File.ReadAllBytes(imagePath));
                Response.End();
        }

        // sample: http://172.21.156.246/xss/TransmitFile
        public void TransmitFile()
        {
            Response.ContentType = "text/html";
            Response.TransmitFile("~/Content/Html/html1.html");
            Response.End();
        }

        // sample: http://172.21.156.246/xss/writeFile
        public void WriteFile()
        {
            Response.ContentType = "text/html";
            Response.WriteFile("~/Content/Html/html1.html");
            Response.End();
        }
        public ActionResult createRaw(string alert)
        {
            string dynamicContent = "<script>" + alert + "('From SendEmail')</script>";
            var htmlHelper = new HtmlHelper(new ViewContext(), new ViewPage());
            // ruleid: csharp_xss_rule-HtmlElementXss
            var rawHtml = htmlHelper.Raw(dynamicContent).ToHtmlString();
            // ruleid: csharp_xss_rule-HtmlElementXss
            Response.Write(rawHtml);
            return Content("Alert shown from htmlHelper.Raw");
        }
    }
}