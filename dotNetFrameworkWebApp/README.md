## dotNetMVS

Used for testing web applications source/sinks based on Dot net MVC.

### Adding tests

- Create a folder under the Controllers folder with the name of the vulnerability category (cookies, endpoint, injection, etc).
- Create a controller with the name of the rule ID related to test cases.
- If necessary, add views under the Views folder with the same vulnerability category name.