﻿using ConsoleApp.utill;
using System;
using System.Xml;
using System.Xml.Xsl;

namespace consoleAppDNF4.CSharp.Other
{
    class UnsafeXSLTSettingUsed : IScenarioRunner
    {
        static void unSafeRead()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("docxml.xml");

            XslCompiledTransform xslt = new XslCompiledTransform();
            // ruleid: csharp_other_rule-UnsafeXSLTSettingUsed
            xslt.Load("docxsl.xslt", new XsltSettings() { EnableDocumentFunction = true }, null);

            string outputPath = "unSafeRead.xml";

            using (XmlWriter writer = XmlWriter.Create(outputPath))
            {
                xslt.Transform(xmlDoc, null, writer);
            }

            Console.WriteLine($"Transformation complete. Output saved to {outputPath}");
        }

        static void unSafeRead2()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("num.xml");

            XslCompiledTransform xslt = new XslCompiledTransform();
            // ruleid: csharp_other_rule-UnsafeXSLTSettingUsed
            xslt.Load("scriptone.xsl", new XsltSettings() { EnableScript = true }, null);

            string outputPath = "unSafeRead2.xml";

            using (XmlWriter writer = XmlWriter.Create(outputPath))
            {
                xslt.Transform(xmlDoc, null, writer);
            }

            Console.WriteLine($"Transformation complete. Output saved to {outputPath}");
        }

        static void safeRead()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("num.xml");

            XslCompiledTransform xslt = new XslCompiledTransform();
            // ok: csharp_other_rule-UnsafeXSLTSettingUsed
            xslt.Load("safestylessheet.xslt", new XsltSettings(), null);

            string outputPath = "safeRead.xml";

            using (XmlWriter writer = XmlWriter.Create(outputPath))
            {
                xslt.Transform(xmlDoc, null, writer);
            }

            Console.WriteLine($"Transformation complete. Output saved to {outputPath}");
        }


        public void run()
        {
            unSafeRead();
            unSafeRead2();
            safeRead();
        }
    }
}
