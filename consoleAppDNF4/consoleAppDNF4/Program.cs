﻿using ConsoleApp.utill;
using System;

namespace consoleAppDNF4
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = "";

            string[] ClassArray = { "consoleAppDNF4.CSharp.Other.UnsafeXSLTSettingUsed", "consoleAppDNF4.CSharp.Injection.XXEInjection" };

            while (!"exit".Equals(input?.ToLower()))
            {
                Console.WriteLine("\n Enter the class name to run \n 'all' - run all classes \n 'list' - get all classes \n 'exit' - quit   : ");
                input = Console.ReadLine()?.ToLower() ?? "";
                {
                    switch (input)
                    {
                        case "exit":
                            {
                                Console.WriteLine("Terminated!!");
                                return;
                            }
                        case "all":
                            {
                                foreach (var item in ClassArray)
                                {
                                    runClass(item);
                                }
                                break;
                            }
                        case "list":
                            {
                                int index = 0;
                                foreach (var item in ClassArray)
                                {
                                    Console.WriteLine($"{index}: {item}");
                                    index++;
                                }
                                break;
                            }
                        default:
                            {
                                try
                                {
                                    runClass(ClassArray[Convert.ToInt16(input)]);
                                }
                                catch (Exception e)
                                {
                                    Console.Write(e);
                                    Console.WriteLine("\nInvalid Input!!");
                                }
                                break;
                            }
                    }
                }

            }

            void runClass(string typeName)
            {
                if (typeName != null)
                {
                    Type _class = Type.GetType(typeName);

                    if (_class != null)
                    {
                        object instance = Activator.CreateInstance(_class);

                        if (instance is IScenarioRunner scenarioRunner)
                        {
                            Console.WriteLine($"Starting {typeName}: {DateTime.Now} \n");
                            scenarioRunner.run();
                            Console.WriteLine($"\nStopped {typeName}: {DateTime.Now}");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Class Not Found : {typeName} ");
                    }
                }
            }
        }
    }
}
