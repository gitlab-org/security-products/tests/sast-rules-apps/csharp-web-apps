<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"  
    xmlns:user="urn:my-scripts">  
  
  <msxsl:script language="C#" implements-prefix="user">  
     <![CDATA[  
     public double area(double side){
       return side * side;  
     }  
      ]]>  
   </msxsl:script>  
  
  <xsl:template match="data">    
  <shapes>  
  
  <xsl:for-each select="shape">  
    <shape>  
    <xsl:copy-of select="node()"/>  
       <area>  
          <xsl:value-of select="user:area(side)"/>   
       </area>  
    </shape>  
  </xsl:for-each>  
  </shapes>  
  </xsl:template>  
</xsl:stylesheet>