<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <xsl:apply-templates select="root/ref"/>
    </xsl:template>

    <xsl:template match="ref">
        <data>
            <xsl:variable name="dataDoc" select="document(xmlfile)"/>
            <xsl:value-of select="$dataDoc/data/dataitem"/>
        </data>
    </xsl:template>
</xsl:stylesheet>