<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <Root>
      <Ele>New Ele</Ele>
      <MDE1>MDE1</MDE1>
      <MDE2>MDE2</MDE2>
      <xsl:copy-of select="*"/>
    </Root>
  </xsl:template>

</xsl:stylesheet>