﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace consoleApp.util.prompt
{
    internal static class PromptReader
    {
        public static string ReadLine(string prompt)
        {
            Console.Write(prompt);
            return Console.ReadLine()?.ToUpper() ?? "";
        }
    }
}
