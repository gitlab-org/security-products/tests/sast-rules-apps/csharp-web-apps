﻿using ConsoleApp.util;
using System.Xml;

namespace consoleApp.cSharp.Injection
{
    internal class XXEInjection : IScenarioRunner
    {
        public void run()
        {
            TestXMlDocument("asserts/xxe_xml.xml");
            TestXMlDocument2("asserts/xxe_unSafe.xml");
            TestReaderSettings("asserts/item-dtd.xml");
            TestReaderWithoutSettings("asserts/xxe_xml.xml");
        }

        private void TestXMlDocument(string path)
        {
            Console.WriteLine("\n*** TestXMlDocument ***\n");
            try
            {
                XmlDocument xmlDocUnSafe = new XmlDocument();
                // ruleid: csharp_injection_rule-XmlDocumentXXEInjection
                xmlDocUnSafe.Load(path);
                Console.WriteLine("with Resolver " + xmlDocUnSafe.InnerText);

                XmlDocument xmlDocSafe = new XmlDocument();
                xmlDocSafe.XmlResolver = null;
                // ok: csharp_injection_rule-XmlDocumentXXEInjection
                xmlDocSafe.Load("null Resolver " + path);
                Console.WriteLine(xmlDocSafe.InnerText);

                XmlDocument xmlDocSafe2 = new XmlDocument { XmlResolver = null };
                // ok: csharp_injection_rule-XmlDocumentXXEInjection
                xmlDocSafe.Load(path);
                Console.WriteLine("null Resolver " + xmlDocSafe.InnerText);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void TestXMlDocument2(string path)
        {
            Console.WriteLine("\n*** TestXMlDocument2 ***\n");
            string xxePayload = $"<!DOCTYPE doc [<!ENTITY win SYSTEM '{path}'>]>"
                     + "<doc>&win;</doc>";
            string xml = "<?xml version='1.0' ?>" + xxePayload;

            try
            {
                XmlDocument xmlDocUnSafe = new XmlDocument();
                // ruleid: csharp_injection_rule-XmlDocumentXXEInjection
                xmlDocUnSafe.LoadXml(xml);
                Console.WriteLine("with Resolver " + xmlDocUnSafe.InnerText);

                XmlDocument xmlDocSafe = new XmlDocument();
                xmlDocSafe.XmlResolver = null;
                // ok: csharp_injection_rule-XmlDocumentXXEInjection
                xmlDocSafe.LoadXml(xml);
                Console.WriteLine("null Resolver " + xmlDocSafe.InnerText);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }


        private void TestReaderSettings(string path)
        {
            Console.WriteLine("\n*** TestReaderSettings ***\n");
            try
            {
                var settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Parse;

                // ruleId: csharp_injection_rule-XmlReaderXXEInjection
                XmlReader reader = XmlReader.Create(path, settings);

                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        Console.WriteLine("Inner XML: " + reader.ReadInnerXml());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void TestReaderSettings2(string path)
        {
            Console.WriteLine("\n*** TestReaderSettings2 ***\n");
            try
            {
                var settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Prohibit;

                // ok: csharp_injection_rule-XmlReaderXXEInjection
                XmlReader reader = XmlReader.Create(path, settings);

                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        Console.WriteLine("Inner XML: " + reader.ReadInnerXml());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void TestReaderSettingsSafe(string path)
        {
            Console.WriteLine("\n*** TestReaderSettingsSafe ***\n");
            try
            {
                var settings = new XmlReaderSettings();
                settings.ProhibitDtd = false;

                // ruleId: csharp_injection_rule-XmlReaderXXEInjection
                XmlReader reader = XmlReader.Create(path, settings);

                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        Console.WriteLine("Inner XML: " + reader.ReadInnerXml());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void TestReaderWithoutSettings(string path)
        {
            Console.WriteLine("\n*** TestReaderWithoutSettings ***\n");
            try
            {
                // ok: csharp_injection_rule-XmlReaderXXEInjection
                XmlReader reader = XmlReader.Create(path);

                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        Console.WriteLine("Inner XML: " + reader.ReadInnerXml());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
