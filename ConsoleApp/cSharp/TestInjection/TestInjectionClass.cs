﻿using ConsoleApp.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.cSharp.TestInjection
{
    class TestInjectionClass : IScenarioRunner
    {
        public void run()
        {
            Console.WriteLine("Test is Running");
        }
    }
}
