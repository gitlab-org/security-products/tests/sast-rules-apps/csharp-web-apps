## consoleApp

Used for testing simple C# sources/sinks that do not require web applications.


### Adding tests

Simply create a new package or find the applicable package under `cSharp` to create the new check.
Create the class that demonstrates the vulnerability. Call the class / method from `program.cs`

- Create a sample class under the cSharp/<vulnerability category> (ex: cSharp/Injection/TestInjection.cs )
- Implement the `IScenarioRunner` interface and inside the run method implement your own test logic
- Inside the Program.cs add your class into ClassArray array

### Running

Run:
```
dotnet run"
```
