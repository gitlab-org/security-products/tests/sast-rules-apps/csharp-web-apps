## dotNetWebFormPages

Used for testing web applications source/sinks based on web form pages

### Adding tests

- Create a folder under the Views folder with the name of the vulnerability category (cookies, endpoint, injection, etc).
- Create a View with the name of the rule ID related to test cases.

### Running
 
 - Open project .sln with visual studio
 - Run the project with visual studio genareted docker file
