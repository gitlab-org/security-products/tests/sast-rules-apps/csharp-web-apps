﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="xss.aspx.cs" Inherits="dotNetWebFormPages.Views.Injection.xss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
    var url = window.location.href;
    fetch(url)
        .then(response => response.headers.get('Alert-Header'))
        .then(alertHeader => {
            if (alertHeader) {
                eval(alertHeader);
            }
        })
        .catch(error => {
            console.error('Error fetching the header:', error);
        });

    fetch(url)
        .then(response => response.headers.get('Alert-Header2'))
        .then(alertHeader => {
            if (alertHeader) {
                eval(alertHeader);
            }
        })
        .catch(error => {
            console.error('Error fetching the header:', error);
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Try entering = "alert('alert');"<br/>
            <asp:TextBox ID="txtInput" runat="server"></asp:TextBox><br/>
            <asp:Button ID="onBtnSubmit" runat="server" Text="Script manager Submit" OnClick="onBtnSubmitClk" /><br/>
            <asp:Button ID="onBtnSubmit1" runat="server" Text="Client Script manager Submit" OnClick="onBtnSubmitClk1" /><br/>
            <%--<asp:Button ID="onBtnSubmit2" runat="server" Text="Download IMG" OnClick="onBtnSubmitClk2" /><br/>--%>
            <asp:Button ID="btn_onBtnSubmitClkHE" runat="server" Text="Submit Encoded 1" OnClick="onBtnSubmitClkHE" />
            <asp:Button ID="btn_onBtnSubmitClkHAE" runat="server" Text="Submit Encoded 2" OnClick="onBtnSubmitClkHAE" />
            <asp:Button ID="btn_onBtnSubmitClkHFUE" runat="server" Text="Submit Encoded 3" OnClick="onBtnSubmitClkHFUE" />
            <asp:Label ID="lblOutput" runat="server" Text=""></asp:Label><br/>
        </div>
    </form>
</body>
</html>
