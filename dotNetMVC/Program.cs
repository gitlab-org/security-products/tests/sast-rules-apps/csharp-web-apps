using dotNetMVC.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<AppDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultDb")));

// ref: csharp_password_rule-PasswordComplexity
// related controller: PasswordComplexityController
builder.Services.AddIdentity<IdentityUser, IdentityRole>(options=> {
    // ok: csharp_password_rule-PasswordComplexity
    options.Password.RequireDigit = true;
    // ok: csharp_password_rule-PasswordComplexity
    options.Password.RequireLowercase = true;
    // ok: csharp_password_rule-PasswordComplexity
    options.Password.RequireNonAlphanumeric = true;
    // ok: csharp_password_rule-PasswordComplexity
    options.Password.RequireUppercase = true;
})
 .AddEntityFrameworkStores<AppDbContext>()
 .AddDefaultTokenProviders();

builder.Services.Configure<IdentityOptions>(options =>
{
    // ruleId: csharp_password_rule-PasswordComplexity
    options.Password.RequiredLength = 5;
    // ruleId: csharp_password_rule-PasswordComplexity
    options.Password.RequiredUniqueChars = 1;
});

builder.Services.AddAntiforgery(options => options.HeaderName = "XSRF-TOKEN");
builder.Services.AddAntiforgery(options => options.FormFieldName = "XSRF-TOKEN");
builder.Services.AddAntiforgery(options => options.SuppressXFrameOptionsHeader = false);

var app = builder.Build();

app.UseAuthentication();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication(); 
app.UseAuthorization();

var antiforgery = app.Services.GetRequiredService<Microsoft.AspNetCore.Antiforgery.IAntiforgery>();

app.Use((context, next) =>
{
    var tokenSet = antiforgery.GetAndStoreTokens(context);
        context.Response.Cookies.Append("XSRF-TOKEN", tokenSet.RequestToken!,
            new CookieOptions { HttpOnly = false });
    
    return next(context);
});

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}");

    endpoints.MapControllerRoute(
        name: "TESTPoint",
        pattern: "Home/TestPoint");
});

app.Run();
