## dotNetMVC

Used for testing web applications source/sinks based on Dot net MVC.

### Adding tests

- Create a folder under the Controllers folder with the name of the vulnerability category (cookies, endpoint, injection, etc).
- Create a controller with the name of the rule ID related to test cases.
- If necessary, add views under the Views folder with the same vulnerability category name.

### Running

Run:

```
docker build -t dotnetmvc . && docker run --rm -p 8080:80 -p 8443:443 dotnetmvc
# Once started, hit the endpoint, alter the url according to your controller name
curl -vvv "http://localhost:8080"
```
