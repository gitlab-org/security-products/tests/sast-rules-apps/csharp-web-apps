﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace TestCSharpRule.Controllers;

public class NonSecuredController : Controller
{
    //curl -i -X POST 'http://localhost:8080/NonSecured/USM1'
    [HttpPost]
    public IActionResult USM1()
    {
        return Ok();
    }

    //Method Secured with ValidateAntiForgeryToken
    //curl -i -c /tmp/ssrf-cookies.txt -X POST 'http://localhost:8080/NonSecured/SM1'
    //-> results in 400 BAD Request
    //-> Use XSRF-TOKEN and cookie data from failed request
    //curl -i -b /tmp/ssrf-cookies.txt --form 'XSRF-TOKEN=<token value>' 'http://localhost:8080/NonSecured/SM1'

    //-> results in 200 OK
    [ValidateAntiForgeryToken]
    [HttpPost]
    public ActionResult SM1()
    {
        return Ok();
    }
}
