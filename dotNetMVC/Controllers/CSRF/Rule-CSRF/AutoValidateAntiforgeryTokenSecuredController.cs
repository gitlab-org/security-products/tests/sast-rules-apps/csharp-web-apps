﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace dotNetMVC.Controllers.CSRF.RuleCSRF;

//Class Secured with AutoValidateAntiforgeryToken
[AutoValidateAntiforgeryToken]
public class AutoValidateAntiforgeryTokenSecuredController : Controller
{
    
    //curl -i -c /tmp/ssrf-cookies.txt -X POST 'http://localhost:8080/AutoValidateAntiforgeryTokenSecured/SM1'
    //-> results in 400 BAD Request
    //-> Use XSRF-TOKEN and cookie data from failed request
    //curl -i -b /tmp/ssrf-cookies.txt --form 'XSRF-TOKEN=<token value>' 'https://localhost:7084/AutoValidateAntiforgeryTokenSecured/SM1'
    //-> results in 200 OK
    [HttpPost]
    public IActionResult SM1(string input)
    {
        return Ok();
    }

    //curl -i -c /tmp/ssrf-cookies.txt -X POST 'http://localhost:8080/AutoValidateAntiforgeryTokenSecured/SM2'
    //-> results in 400 BAD Request
    //-> Using XSRF-TOKEN and cookie data from failed request
    //curl -i -b /tmp/ssrf-cookies.txt --form 'XSRF-TOKEN=<token value>' 'http://localhost:8080/AutoValidateAntiforgeryTokenSecured/SM2'
    //-> results in 200 OK
    [ValidateAntiForgeryToken]
    [HttpPost]
    public ActionResult SM2(string input)
    {
        return Ok();
    }
}
