using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;

// ref: csharp_password_rule-PasswordComplexity
// related testsaces are inside programe.cs
// sample end point: https://localhost:7084/PasswordComplexity
namespace dotNetMVC.Controllers.Password
{
    public class PasswordComplexityController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;

        public PasswordComplexityController(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            return View("/Views/Password/PasswordComplexity/Index.cshtml");
        }

        public async Task<IActionResult> VerifyUser(string password)
        {
            var user = new IdentityUser { UserName = "Testsas" };
            try
            {
                var result = await _userManager.CreateAsync(user, password);
                if (!result.Succeeded)
                {
                    string errorStr = "Password validetion Failed\n";
                    foreach (var item in result.Errors)
                    {
                        errorStr = $"- {item.Description}";
                    }
                    TempData["message"] = errorStr;
                }

            }
            catch (SqlException ex)
            {
                // SqlException trown due to invalid sql connection password validetion is passed at this point
                TempData["message"] = "Password validetion passed";
            }
            
            return RedirectToAction("Index", "PasswordComplexity");
        }
    }
}
